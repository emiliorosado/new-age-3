package br.org.emilio.newage3.dto;

import java.util.Date;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClienteDTO {

	private Long codigo;
	
	@NotNull(message = "N�mero CPF/CNPJ deve ser preenchido")
	@Positive(message = "N�mero CPF/CNPJ deve ser preenchido")
	private Long numeroCpfCnpj;
	
	@NotBlank(message = "Nome deve ser preenchido")
	@Size(min = 5, message = "Nome deve conter m�nimo 5 caracteres")
	private String nome;
	
	@NotBlank(message = "Endere�o deve ser preenchido")
	private String endereco;

	@NotNull(message = "N�mero Telefone deve ser preenchido")
	@Positive(message = "N�mero Telefone deve ser preenchido")
	private Long telefone;

	@NotNull(message = "Valor Contrato deve ser preenchido")
	@Positive(message = "Valor Contrato deve ser maior que zero")
	private Double valorContrato;
	
	private Date dataCadastro;
	
	private Date dataHoraAtualizacao;
	
}
