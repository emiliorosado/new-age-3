package br.org.emilio.newage3.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReturnDTO {
	
	private boolean success;
	private List<MessageDTO> errors;
	private List<MessageDTO> warnings;
	private Object data;
	
}
