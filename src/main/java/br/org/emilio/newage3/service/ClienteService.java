package br.org.emilio.newage3.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.org.emilio.newage3.dto.ClienteDTO;
import br.org.emilio.newage3.dto.ReturnDTO;
import br.org.emilio.newage3.entity.ClienteEntity;
import br.org.emilio.newage3.repository.ClienteRepository;
import br.org.emilio.newage3.util.AutoincUtil;
import br.org.emilio.newage3.util.ReturnMessageUtil;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository repository;

	@Autowired
	private AutoincUtil autoinc;
	
	private ReturnMessageUtil msg = new ReturnMessageUtil();
	
	@Transactional
	public ReturnDTO insert(ClienteDTO dto) {
		ReturnDTO ret = new ReturnDTO();
		
		long codigo = this.autoinc.nextVal(ClienteEntity.class);
		ClienteEntity entity = this.repository.get(codigo);
		if (entity != null) {
			this.msg.addError(ret, "C�digo j� existe");
			return ret;
		}

		entity = this.toEntity(dto);
		entity.setCodigo(codigo);
		entity.setDataCadastro(new Date());
		entity.setDataHoraAtualizacao(new Date());
		this.repository.save(entity);

		dto = this.toDTO(entity);
		
		ret.setSuccess(true);
		ret.setData(dto);
		return ret;
	}
	
	
	@Transactional
	public ReturnDTO update(long codigo, ClienteDTO dto) {
		ReturnDTO ret = new ReturnDTO();

		ClienteEntity entity = this.repository.get(codigo);
		if (entity == null) {
			this.msg.addError(ret, "C�digo n�o encontrado");
			return ret;
		}
		
		entity = this.toEntity(dto, entity);
		entity.setDataHoraAtualizacao(new Date());
		this.repository.save(entity);
		
		dto = this.toDTO(entity);
		
		ret.setSuccess(true);
		ret.setData(dto);
		return ret;
	}
	
	
	@Transactional
	public ReturnDTO delete(long codigo) {
		ReturnDTO ret = new ReturnDTO();

		ClienteEntity entity = this.repository.get(codigo);
		if (entity == null) {
			this.msg.addError(ret, "C�digo n�o encontrado");
			return ret;
		}
		
		this.repository.delete(entity);
		
		ret.setSuccess(true);
		return ret;
	}
	
	
	public ReturnDTO getByCodigo(long codigo) {
		ReturnDTO ret = new ReturnDTO();

		ClienteEntity entity = this.repository.get(codigo);
		if (entity == null) {
			this.msg.addError(ret, "C�digo n�o encontrado");
			return ret;
		}
		
		ClienteDTO dto = this.toDTO(entity);
		
		ret.setSuccess(true);
		ret.setData(dto);
		return ret;
	}
	
	
	public ReturnDTO getByCpfCnpj(long cpfCnpj) {
		ReturnDTO ret = new ReturnDTO();

		ClienteEntity entity = this.repository.getByCpfCnpj(cpfCnpj);
		if (entity == null) {
			this.msg.addError(ret, "CPF/CNPJ n�o encontrado");
			return ret;
		}
		
		ClienteDTO dto = this.toDTO(entity);

		ret.setSuccess(true);
		ret.setData(dto);
		return ret;
	}
	
	
	public ReturnDTO getByNome(String nome) {
		ReturnDTO ret = new ReturnDTO();

		List<ClienteDTO> list = this.toList(this.repository.getByNome(nome));

		ret.setSuccess(true);
		ret.setData(list);
		return ret;
	}
	
	
	public ReturnDTO getAll() {
		ReturnDTO ret = new ReturnDTO();

		List<ClienteDTO> list = this.toList(this.repository.getAll());

		ret.setSuccess(true);
		ret.setData(list);
		return ret;
	}
	
	
	private List<ClienteDTO> toList(List<ClienteEntity> listEntity) {
		List<ClienteDTO> list = new ArrayList<ClienteDTO>();
		
		for (ClienteEntity entity : listEntity) {
			ClienteDTO dto = new ClienteDTO();
			dto.setCodigo(entity.getCodigo());
			dto.setNome(entity.getNome());
			list.add(dto);
		}
		
		return list;
	}
	
	private ClienteEntity toEntity(ClienteDTO dto, ClienteEntity entity) {
		entity.setNome(dto.getNome());
		entity.setNumeroCpfCnpj(dto.getNumeroCpfCnpj());
		entity.setTelefone(dto.getTelefone());
		entity.setEndereco(dto.getEndereco());
		entity.setValorContrato(dto.getValorContrato());
		
		return entity;
	}
	
	private ClienteEntity toEntity(ClienteDTO dto) {
		return this.toEntity(dto, new ClienteEntity());
	}
	
	private ClienteDTO toDTO(ClienteEntity entity) {
		ClienteDTO dto = new ClienteDTO();
		
		dto.setCodigo(entity.getCodigo());
		dto.setNome(entity.getNome());
		dto.setNumeroCpfCnpj(entity.getNumeroCpfCnpj());
		dto.setTelefone(entity.getTelefone());
		dto.setEndereco(entity.getEndereco());
		dto.setValorContrato(entity.getValorContrato());
		dto.setDataCadastro(entity.getDataCadastro());
		dto.setDataHoraAtualizacao(entity.getDataHoraAtualizacao());
		
		return dto;
	}

}
