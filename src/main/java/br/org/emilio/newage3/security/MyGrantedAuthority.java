package br.org.emilio.newage3.security;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MyGrantedAuthority implements GrantedAuthority {
	
	private static final long serialVersionUID = 1L;
	
	private String authority;

}
