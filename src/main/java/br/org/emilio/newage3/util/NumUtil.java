package br.org.emilio.newage3.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

import org.apache.commons.lang3.math.NumberUtils;

public final class NumUtil extends NumberUtils {

	private NumUtil() {
	}
	
	
	private static DecimalFormat createDecimalFormat(String pattern, char decimalSeparator, char groupingSeparator) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator(decimalSeparator);
		symbols.setGroupingSeparator(groupingSeparator);
		return new DecimalFormat(pattern, symbols);
	}
	
	
	public static String format(double number, String pattern, char decimalSeparator, char groupingSeparator) {
		DecimalFormat df = createDecimalFormat(pattern, decimalSeparator, groupingSeparator);
		return df.format(number);
	}
	
	
	public static String format(double number, String pattern) {
		return format(number, pattern, ',', '.');
	}
	
	
	private static Number parse(String num, String pattern, char decimalSeparator, char groupingSeparator) {
		DecimalFormat df = createDecimalFormat(pattern, decimalSeparator, groupingSeparator);
		Number number;
		
		try {
			number = df.parse(num);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		
		return number;
	}
	
	
	public static double parseDouble(String number, String pattern, char decimalSeparator, char groupingSeparator) {
		return parse(number, pattern, decimalSeparator, groupingSeparator).doubleValue();
	}
	
	
	public static double parseDouble(String number, String pattern) {
		return parseDouble(number, pattern, ',', '.');
	}
	
	
	public static float parseFloat(String number, String pattern, char decimalSeparator, char groupingSeparator) {
		return parse(number, pattern, decimalSeparator, groupingSeparator).floatValue();
	}
	
	
	public static float parseFloat(String number, String pattern) {
		return parseFloat(number, pattern, ',', '.');
	}
	
	
	public static long parseLong(String number, String pattern, char decimalSeparator, char groupingSeparator) {
		return parse(number, pattern, decimalSeparator, groupingSeparator).longValue();
	}
	
	
	public static long parselong(String number, String pattern) {
		return parseLong(number, pattern, ',', '.');
	}
	
	
	public static int parseInt(String number, String pattern, char decimalSeparator, char groupingSeparator) {
		return parse(number, pattern, decimalSeparator, groupingSeparator).intValue();
	}
	
	
	public static long parseInt(String number, String pattern) {
		return parseInt(number, pattern, ',', '.');
	}
	
	
	public static boolean isEmpty(Double num) {
		if (num == null || num.equals(0.0)) {
			return true;
		}
		
		return false;
	}
	
}
