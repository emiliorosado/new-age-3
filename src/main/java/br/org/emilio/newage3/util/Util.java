package br.org.emilio.newage3.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class Util {

	private Util() {
	}


	public static String md5(String texto) {  
		MessageDigest md;
		BigInteger hash;
		
		try {
			md = MessageDigest.getInstance("MD5");
			hash = new BigInteger(1, md.digest(texto.getBytes()));  
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
		
		return hash.toString(16);              
	}


	public static String noMask(String texto, boolean letra) {
		String novotexto = "";

		for ( int i = 0 ; i < texto.length() ; i++ ) {
			String scarac = texto.substring(i, i+1).toUpperCase();
			char carac = scarac.charAt(0);

			if (carac >= '0' && carac <= '9') {
				novotexto = novotexto + carac;
			} else if (letra && carac >= 'A' && carac <= 'Z') {
				novotexto = novotexto + carac;
			}
		}

		return novotexto;
	}


	public static String noMask(String texto) {
		return noMask(texto, false);
	}


	public static String mask(String texto, String masc, boolean direita, boolean letra) {
		String texto1 = noMask(texto, letra);
		String novotexto = "";

		if (!direita) {
			int posTxt = 0;

			for ( int i = 0 ; i < masc.length() ; i++ ) {
				if (posTxt < texto.length()) {
					String charMasc = masc.substring(i, i+1);
					String charTxt = texto1.substring(posTxt, posTxt+1);

					if (charMasc.equals("#")) {
						novotexto = novotexto + charTxt;
						posTxt++;
					} else {
						novotexto = novotexto + charMasc;
					}
				}
			}
		} else {
			int posTxt = texto.length() - 1;

			for ( int i = masc.length() - 1 ; i >= 0 ; i-- ) {
				if (posTxt >= 0) {
					String charMasc = masc.substring(i, i+1);
					String charTxt = texto1.substring(posTxt, posTxt+1);

					if (charMasc.equals("#")) {
						novotexto = charTxt + novotexto;
						posTxt--;
					} else {
						novotexto = charMasc + novotexto;
					}
				}
			}
		}

		return novotexto;
	}


	public static String mask(String texto, String masc, boolean direita) {
		return mask(texto, masc, direita, false);
	}


	public static String mask(String texto, String masc) {
		return mask(texto, masc, false);
	}


	public static boolean validEmail(String email) {
		if (email == null || email.length() == 0) {
			return false;
		}

		if (email.charAt(0) == '@' || email.charAt(email.length() - 1) == '@') {
			return false;
		}

		int arroba = 0;

		for ( int i = 0 ; i < email.length() ; i++ ) {
			char carac = email.charAt(i);
			int codi = carac;

			if (carac == '@') {
				arroba++;
			} else if (codi < 33 || codi > 122) {
				return false;
			} else if (carac == '\'' || carac == '"' || carac == '`' || carac == '<' ||
					carac == '>' || carac == '\\' || carac == '&') {
				return false;
			}
		}

		if (arroba != 1) {
			return false;
		}

		return true;
	}


	public static boolean validCPF(String cpf) {
		if (cpf.length() != 11) {
			return false;
		}

		for ( int i = 0 ; i < 11 ; i++ ) {
			char num = cpf.charAt(i);
			if (num < '0' || num > '9') {
				return false;
			}
		}

		int d1 = 0;
		int m = 2;

		for ( int i = 8 ; i >= 0 ; i-- ) {
			d1 = d1 + (cpf.charAt(i) * m);
			m++;
		}

		d1 = 11 - (d1 % 11);
		if (d1 >= 10)
			d1 = 0;

		int d2 = d1 * 2;
		m = 3;

		for ( int i = 8 ; i >= 0 ; i-- ) {
			d2 = d2 + (cpf.charAt(i) * m);
			m++;
		}

		d2 = 11 - (d2 % 11);
		if (d2 >= 10)
			d2 = 0;

		String calculado = "" + d1 + d2;
		String digitado = cpf.substring(9);

		if (!calculado.equals(digitado)) {
			return false;
		}

		return true;
	}


	public static boolean validCNPJ(String cnpj) {
		if (cnpj.length() != 14) {
			return false;
		}

		for ( int i = 0 ; i < 14 ; i++ ) {
			char num = cnpj.charAt(i);
			if (num < '0' || num > '9') {
				return false;
			}
		}

		int d1 = 0;
		int m = 2;

		for ( int i = 11 ; i >= 0 ; i-- ) {
			d1 = d1 + (cnpj.charAt(i) * m);
			m++;
			if (m >= 10)
				m = 2;
		}

		d1 = 11 - (d1 % 11);
		if (d1 >= 10)
			d1 = 0;

		int d2 = d1 * 2;
		m = 3;

		for ( int i = 11 ; i >= 0 ; i-- ) {
			d2 = d2 + (cnpj.charAt(i) * m);
			m++;
			if (m >= 10)
				m = 2;
		}

		d2 = 11 - (d2 % 11);
		if (d2 >= 10)
			d2 = 0;

		String calculado = "" + d1 + d2;
		String digitado = cnpj.substring(12);

		if (!calculado.equals(digitado)) {
			return false;
		}

		return true;
	}
	
}
