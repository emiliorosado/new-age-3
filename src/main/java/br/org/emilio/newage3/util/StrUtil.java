package br.org.emilio.newage3.util;

import org.apache.commons.lang3.StringUtils;

public final class StrUtil extends StringUtils {

	private StrUtil() {
	}
	
	
	public static boolean isEmptyBlank(String str) {
		if (!isEmpty(str) || !isBlank(str)) {
			return false;
		}
		
		return true;
	}
	
	
	public static boolean isNotEmptyBlank(String str) {
		return !isEmptyBlank(str);
	}
	
}
