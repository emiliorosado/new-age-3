package br.org.emilio.newage3.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.org.emilio.newage3.entity.AutoincEntity;
import br.org.emilio.newage3.entity.AutoincEntityId;
import br.org.emilio.newage3.repository.AutoincRepository;
import jakarta.persistence.Table;

@Service
public class AutoincUtil {

	@Autowired
	private AutoincRepository repository;
	
	
	public long currentVal(Class<?> clazz, int sequencia) {
		AutoincEntity autoinc = this.repository.get(this.nomeTabela(clazz), sequencia);
		
		if (autoinc == null) {
			return 0;
		}
		
		return autoinc.getAtual();
	}
	
	
	public long currentVal(Class<?> clazz) {
		return this.currentVal(clazz, 1);
	}	
	
	
	public long nextValNoUpdate(Class<?> clazz, int sequencia) {
		AutoincEntity autoinc = this.repository.get(this.nomeTabela(clazz), sequencia);
		
		if (autoinc == null) {
			return 1;
		}
		
		return autoinc.getAtual() + 1;
	}
	
	
	public long nextValNoUpdate(Class<?> clazz) {
		return this.nextValNoUpdate(clazz, 1);
	}
	
	
	public long nextVal(Class<?> clazz, int sequencia) {
		String tabela = this.nomeTabela(clazz);
		AutoincEntity autoinc = this.repository.get(tabela, sequencia);
		
		if (autoinc == null) {
			autoinc = new AutoincEntity();
			
			autoinc.setId(new AutoincEntityId());
			autoinc.getId().setTabela(tabela);
			autoinc.getId().setSequencia(sequencia);
			autoinc.setInicio(1);
			autoinc.setFim(999999999L);
			autoinc.setAtual(0L);
			
			this.repository.save(autoinc);
		}

		long proximo = autoinc.getAtual() + 1;
		if (proximo > autoinc.getFim()) {
			proximo = autoinc.getInicio();
		}

		autoinc.setAtual(proximo);
		this.repository.save(autoinc);
		
		return proximo;
	}
	
	
	public long nextVal(Class<?> clazz) {
		return this.nextVal(clazz, 1);
	}
	
	
	private String nomeTabela(Class<?> clazz) {
		String tabela;
		
		if (clazz.isAnnotationPresent(Table.class)) {
			tabela = clazz.getAnnotation(Table.class).name();
		} else {
			tabela = clazz.getName();
		}
		
		return tabela;
	}
	
}
