package br.org.emilio.newage3.util;

import java.util.ArrayList;
import java.util.List;

import br.org.emilio.newage3.dto.MessageDTO;
import br.org.emilio.newage3.dto.ReturnDTO;

public class ReturnMessageUtil {
	
	public void addMessage(List<MessageDTO> messages, int id, String message) {
		if (messages == null) {
			messages = new ArrayList<MessageDTO>();
		}
		
		MessageDTO messageDto = new MessageDTO();
		messageDto.setId(id);
		messageDto.setMessage(message);
		
		messages.add(messageDto);
	}
	
	public void addMessage(List<MessageDTO> messages, String message) {
		if (messages == null) {
			messages = new ArrayList<MessageDTO>();
		}

		this.addMessage(messages, messages.size() + 1, message);
	}
	
	public void addError(ReturnDTO returnDto, int id, String message) {
		returnDto.setSuccess(false);
		returnDto.setData(null);
		this.addMessage(returnDto.getErrors(), id, message);
	}
	
	public void addError(ReturnDTO returnDto, String message) {
		returnDto.setSuccess(false);
		returnDto.setData(null);
		this.addMessage(returnDto.getErrors(), message);
	}
	
	public void addWarning(ReturnDTO returnDto, int id, String message) {
		this.addMessage(returnDto.getWarnings(), id, message);
	}
	
	public void addWarning(ReturnDTO returnDto, String message) {
		this.addMessage(returnDto.getWarnings(), message);
	}
	
}
