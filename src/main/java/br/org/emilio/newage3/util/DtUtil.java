package br.org.emilio.newage3.util;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

public final class DtUtil extends DateUtils {

	private DtUtil() {
	}
	
	
	public static String format(Date date, String pattern) {
		return DateFormatUtils.format(date, pattern);
	}
	
	
	public static String formatBR(Date date) {
		return DateFormatUtils.format(date, "dd/MM/yyyy");
	}
	
	
	public static String formatGregorian(Date date) {
		return DateFormatUtils.format(date, "yyyy-MM-dd");
	}
	
	
	public static Date parse(String dt, String ptt) {
		Date date;
		String[] patterns = {ptt};
		
		try {
			date = parseDate(dt, patterns);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		
		return date;
	}
	
	
	public static Date parseBR(String date) {
		return parse(date, "dd/MM/yyyy");
	}
	
	
	public static Date parseGregorian(String date) {
		return parse(date, "yyyy-MM-dd");
	}
	
	
	public static boolean valid(String dt, String ptt) {
		String[] patterns = {ptt};
		
		try {
			parseDate(dt, patterns);
		} catch (ParseException e) {
			return false;
		}
		
		return true;
	}
	
	
	public static boolean validBR(String date) {
		return valid(date, "dd/MM/yyyy");
	}
	
	
	public static boolean validGregorian(String date) {
		return valid(date, "yyyy-MM-dd");
	}
	
	
	public static Date currentDate() {
		String fmt = "yyyyMMdd";
		return parse(format(new Date(), fmt), fmt);
	}
	
	
	public static Date truncate(Date date) {
		String fmt = "yyyyMMdd";
		return parse(format(date, fmt), fmt);
	}


	public static int lastDay(Date date) {
		int mes = month(date);
		int ano = year(date);

		if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
			return 31;
		else if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
			return 30;
		else if ((ano % 4) == 0)
			return 29;

		return 28;
	}


	public static int day(Date date) {
		return Integer.parseInt(format(date, "dd"), 10);
	}


	public static int month(Date date) {
		return Integer.parseInt(format(date, "MM"), 10);
	}


	public static String monthName(Date date) {
		int mes = month(date);
		String[] meses = {"Janeiro", "Fevereiro", "Mar�o", "Abril", "Maio", "Junho",
							"Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"};
		return meses[mes - 1];
	}


	public static int year(Date date) {
		return Integer.parseInt(format(date, "yyyy"), 10);
	}


	public static int week(Date date) {
		return Integer.parseInt(format(date, "u"), 10);
	}


	public static String weekName(Date date) {
		int dia = week(date);
		String[] semana = {"Segunda", "Ter�a", "Quarta", "Quinta", "Sexta", "S�bado", "Domingo"};
		return semana[dia - 1];
	}


	public static int diffDays(Date date1, Date date2) {
		Date dataInicio;
		Date dataFim;

		if (date1.getTime() < date2.getTime()) {
			dataInicio = new Date(date1.getTime());
			dataFim = new Date(date2.getTime());
		} else {
			dataInicio = new Date(date2.getTime());
			dataFim = new Date(date1.getTime());
		}

		int diff = 0;

		while (dataInicio.getTime() < dataFim.getTime()) {
			dataInicio = addDays(dataInicio, 1);
			if (dataInicio.getTime() < dataFim.getTime()) {
				diff++;
			}
		}

		return diff;
	}


	public static int diffMonths(Date date1, Date date2) {
		Date dataInicio;
		Date dataFim;

		if (date1.getTime() < date2.getTime()) {
			dataInicio = new Date(date1.getTime());
			dataFim = new Date(date2.getTime());
		} else {
			dataInicio = new Date(date2.getTime());
			dataFim = new Date(date1.getTime());
		}

		int diff = 0;

		while (dataInicio.getTime() < dataFim.getTime()) {
			dataInicio = addMonths(dataInicio, 1);
			if (dataInicio.getTime() <= dataFim.getTime()) {
				diff++;
			}
		}

		return diff;
	}


	public static int diffYears(Date date1, Date date2) {
		Date dataInicio;
		Date dataFim;

		if (date1.getTime() < date2.getTime()) {
			dataInicio = new Date(date1.getTime());
			dataFim = new Date(date2.getTime());
		}
		else {
			dataInicio = new Date(date2.getTime());
			dataFim = new Date(date1.getTime());
		}

		int diff = 0;

		while (dataInicio.getTime() < dataFim.getTime()) {
			dataInicio = addYears(dataInicio, 1);
			if (dataInicio.getTime() <= dataFim.getTime()) {
				diff++;
			}
		}

		return diff;
	}


	public static Date minDate() {
		return parse("1000-01-01", "yyyy-MM-dd");
	}


	public static Date maxDate() {
		return parse("9999-12-31", "yyyy-MM-dd");
	}
	
}
