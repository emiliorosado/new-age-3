package br.org.emilio.newage3.entity;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "TB_CLIENTE")
public class ClienteEntity {

	@Id
	@Column(name = "CD_CLIENTE")
	private Long codigo;

	@Column(name = "NR_CPF_CNPJ")
	private Long numeroCpfCnpj;

	@Column(name = "NM_CLIENTE")
	private String nome;

	@Column(name = "ENDERECO")
	private String endereco;

	@Column(name = "TELEFONE")
	private Long telefone;

	@Column(name = "VL_CONTRATO")
	private Double valorContrato;

	@Column(name = "DT_CADASTRO")
	@Temporal(TemporalType.DATE)
	private Date dataCadastro;

	@Column(name = "DH_ATUALIZACAO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHoraAtualizacao;
	
}
