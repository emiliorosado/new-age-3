package br.org.emilio.newage3.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class AutoincEntityId {

	@Column(name = "TABELA")
	private String tabela;

	@Column(name = "SEQUENCIA")
	private Integer sequencia;
}
