package br.org.emilio.newage3.entity;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "AUTOINC")
public class AutoincEntity {

	@EmbeddedId
	private AutoincEntityId id;

	@Column(name = "INICIO")
	private Integer inicio;

	@Column(name = "FIM")
	private Long fim;

	@Column(name = "ATUAL")
	private Long atual;
	
}
