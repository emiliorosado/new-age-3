package br.org.emilio.newage3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewAge3Application {

    public static void main(String[] args) {
        SpringApplication.run(NewAge3Application.class, args);
    }
    
}
