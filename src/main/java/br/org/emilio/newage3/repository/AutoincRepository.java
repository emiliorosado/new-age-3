package br.org.emilio.newage3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.org.emilio.newage3.entity.AutoincEntity;
import br.org.emilio.newage3.entity.AutoincEntityId;

public interface AutoincRepository extends JpaRepository<AutoincEntity, AutoincEntityId>{

	@Query(
		"from AutoincEntity " +
		"where id.tabela = ?1 " +
		"and id.sequencia = ?2 ")
	public AutoincEntity get(String tabela, int sequencia);
	
}
