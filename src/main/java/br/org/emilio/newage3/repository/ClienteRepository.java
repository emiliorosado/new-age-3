package br.org.emilio.newage3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.org.emilio.newage3.entity.ClienteEntity;

public interface ClienteRepository extends JpaRepository<ClienteEntity, Integer>{
	
	public ClienteEntity findByCodigo(Integer codigo);
	
	public ClienteEntity findFirstByNumeroCpfCnpj(Long numeroCpfCnpj);

	@Query("from ClienteEntity where nome like '%' || trim(?1) || '%'")
	public List<ClienteEntity> findByNome(String nome);
	
	@Query(
		"from ClienteEntity " +
		"where codigo = ?1")
	public ClienteEntity get(Long codigo);
	
	@Query(
		"from ClienteEntity " +
		"where numeroCpfCnpj = ?1")
	public ClienteEntity getByCpfCnpj(Long cpfCnpj);
	
	@Query(
		"from ClienteEntity " +
		"where nome like '%' || trim(?1) || '%' " +
		"order by nome")
	public List<ClienteEntity> getByNome(String nome);
	
	@Query(
		"from ClienteEntity " +
		"order by nome")
	public List<ClienteEntity> getAll();
	
}
