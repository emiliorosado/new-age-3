package br.org.emilio.newage3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.org.emilio.newage3.dto.ClienteDTO;
import br.org.emilio.newage3.dto.ReturnDTO;
import br.org.emilio.newage3.service.ClienteService;
import jakarta.validation.Valid;

@RestController
@Secured("CLIENTE")
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	private ClienteService service;
	
	
	@Secured("CLIENTE_WRITE")
	@PostMapping
	public ReturnDTO insert(@Valid @RequestBody ClienteDTO dto) {
		return this.service.insert(dto);
	}
	
	
	@Secured("CLIENTE_WRITE")
	@PutMapping("/{codigo}")
	public ReturnDTO update(@PathVariable("codigo") int codigo, @Valid @RequestBody ClienteDTO dto) {
		return this.service.update(codigo, dto);
	}
	
	
	@Secured("CLIENTE_WRITE")
	@DeleteMapping("/{codigo}")
	public ReturnDTO delete(@PathVariable("codigo") int codigo) {
		return this.service.delete(codigo);
	}
	
	
	@Secured("CLIENTE")
	@GetMapping("/{codigo}")
	public ReturnDTO getByCodigo(@PathVariable("codigo") int codigo) {
		return this.service.getByCodigo(codigo);
	}
	
	
	@Secured("CLIENTE")
	@GetMapping("/cpfcnpj/{cpfCnpj}")
	public ReturnDTO getByCpfCnpj(@PathVariable("cpfCnpj") long cpfCnpj) {
		return this.service.getByCpfCnpj(cpfCnpj);
	}
	
	
	@Secured("CLIENTE")
	@GetMapping("/nome/{nome}")
	public ReturnDTO getByNome(@PathVariable("nome") String nome) {
		return this.service.getByNome(nome);
	}
	
	
	@Secured("CLIENTE")
	@GetMapping("/all")
	public ReturnDTO getAll() {
		return this.service.getAll();
	}
	
}
