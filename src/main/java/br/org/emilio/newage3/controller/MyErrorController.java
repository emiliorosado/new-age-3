package br.org.emilio.newage3.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.org.emilio.newage3.dto.MessageDTO;
import br.org.emilio.newage3.dto.ReturnDTO;

@RestControllerAdvice
public class MyErrorController {
	
	private static Logger LOGGER = LoggerFactory.getLogger(MyErrorController.class);

	@ExceptionHandler(Exception.class)
	public ReturnDTO handleExceptions(Exception e) {
		List<MessageDTO> errors = new ArrayList<MessageDTO>();

		if (e instanceof MethodArgumentNotValidException) {
			int i = 0;
			for (FieldError error : ((MethodArgumentNotValidException) e).getBindingResult().getFieldErrors()) {
				i++;
				MessageDTO message = new MessageDTO();
				message.setId(i);
				message.setMessage(error.getDefaultMessage());
				errors.add(message);
			}
		} else {
			LOGGER.error("Erro na chamada do servico", e);
			
			MessageDTO message = new MessageDTO();
			message.setId(999);
			message.setMessage("SISTEMA INDISPONIVEL");
			errors.add(message);
		}
		
		ReturnDTO ret = new ReturnDTO();
		ret.setSuccess(false);
		ret.setErrors(errors);
		
		return ret;
	}
	
}
